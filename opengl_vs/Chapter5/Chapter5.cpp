#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include "sb7.h"
#include <math.h>
#include<string>
#include<fstream>
#include<Vector.h>
#include "FileIO.h"
#include "sb7ext.h"
#include "vmath.h"

class MyApplication : public sb7::application
{
public:

	virtual void init()
	{
		sb7::application::init();
		strcpy(this->info.title, "Chapter 5 Tutorial");
	}

	virtual void startup()
	{
		static const float data[] =
		{
			0.25, -0.25, 0.5, 1.0,
			-0.25, -0.25, 0.5, 1.0,
			0.25, 0.25, 0.5, 1.0
		};

		GLuint buffer;
		glCreateBuffers(1, &buffer);

		glBindBuffer(GL_ARRAY_BUFFER, buffer);
		
		glNamedBufferStorage(buffer, 1024 * 1024, nullptr, GL_DYNAMIC_STORAGE_BIT | GL_MAP_WRITE_BIT | GL_MAP_READ_BIT);

		void *ptr = glMapNamedBuffer(buffer, GL_READ_WRITE);
		memcpy(ptr, data, sizeof(data));
		//glUnmapNamedBuffer(GL_ARRAY_BUFFER);

		void *otherPtr = glMapNamedBuffer(buffer, GL_READ_WRITE);

		m_Program = this->compileShaders();
		//glCreateVertexArrays(1, &m_VertexArrayObject);
		glGenVertexArrays(1, &m_VertexArrayObject);
		glBindVertexArray(m_VertexArrayObject);

		glPointSize(5.0f);
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}

	virtual void shutdown()
	{
		glDeleteVertexArrays(1, &m_VertexArrayObject);
		glDeleteProgram(m_Program);
	}

	virtual void render(double currentTime)
	{
		const GLfloat color[] = { (float)sin(currentTime)*.5f+.5f,
			(float)cos(currentTime)*.5f + .5f, 0.0f, 1.0f };
		glClearBufferfv(GL_COLOR, 0, color);

		glUseProgram(m_Program);

		GLfloat attribute[] = 
		{
			(float)sin(currentTime) *.5f,
			(float)cos(currentTime)*.6f,
			0.0f, 0.0f
		};

		const GLfloat inputColor[] = { 0.0f, 0.0f, 1.0f, 1.0f };

		glVertexAttrib4fv(0, attribute);
		glVertexAttrib4fv(1, inputColor);

		//glPatchParameteri(GL_PATCH_VERTICES, 32);

		glDrawArrays(GL_PATCHES, 0, 3);

	}



	GLuint compileShaders()
	{
		GLuint vertexShader;
		GLuint tesselationControlShader;
		GLuint tesselationEvaluationShader;
		GLuint geometryShader;
		GLuint fragmentShader;
		GLuint program;

		char szVSSource[10240];
		if (!GApi::File::LoadFile("shader.vs", szVSSource, 10240))
			return -1;
		const GLchar* pszVertexShaderSource = szVSSource;

		char szTSCSSource[10240];
		if (!GApi::File::LoadFile("shader.tcs", szTSCSSource, 10240))
			return -1;
		const GLchar* pszTessellationShaderSource = szTSCSSource;

		char szTSESSource[10240];
		if (!GApi::File::LoadFile("shader.tes", szTSESSource, 10240))
			return -1;
		const GLchar* pszTessellationEvaluationShaderSource = szTSESSource;

		char szGSSource[10240];
		if (!GApi::File::LoadFile("shader.gs", szGSSource, 10240))
			return -1;
		const GLchar* pszGeometryShaderSource = szGSSource;

		char szFSSource[10240];
		if (!GApi::File::LoadFile("shader.fs", szFSSource, 10240))
			return -1;
		const GLchar* pszFragmentShaderSource = szFSSource;

		vertexShader = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertexShader, 1, &pszVertexShaderSource, NULL);
		glCompileShader(vertexShader);

		tesselationControlShader = glCreateShader(GL_TESS_CONTROL_SHADER);
		glShaderSource(tesselationControlShader, 1, &pszTessellationShaderSource, NULL);
		glCompileShader(tesselationControlShader);

		tesselationEvaluationShader = glCreateShader(GL_TESS_EVALUATION_SHADER);
		glShaderSource(tesselationEvaluationShader, 1, &pszTessellationEvaluationShaderSource, NULL);
		glCompileShader(tesselationEvaluationShader);

		geometryShader = glCreateShader(GL_GEOMETRY_SHADER);
		glShaderSource(geometryShader, 1, &pszGeometryShaderSource, NULL);
		glCompileShader(geometryShader);

		fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragmentShader, 1, &pszFragmentShaderSource, NULL);
		glCompileShader(fragmentShader);

		program = glCreateProgram();
		glAttachShader(program, vertexShader);
		glAttachShader(program, tesselationControlShader);
		glAttachShader(program, tesselationEvaluationShader);
		glAttachShader(program, geometryShader);
		glAttachShader(program, fragmentShader);

		glLinkProgram(program);

		glDeleteShader(vertexShader);
		glDeleteShader(tesselationControlShader);
		glDeleteShader(tesselationEvaluationShader);
		glDeleteShader(geometryShader);
		glDeleteShader(fragmentShader);

		return program;
	}

private:
	GLuint m_Program;
	GLuint m_VertexArrayObject;
};

DECLARE_MAIN(MyApplication);

