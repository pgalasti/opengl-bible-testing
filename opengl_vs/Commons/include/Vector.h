#ifndef GALASTI_VECTOR_H
#define GALASTI_VECTOR_H

#include "stdinc.h"

namespace GApi {
	namespace Math {


		struct DllExport Vector4D
		{
			union
			{
				float xyza[4];
				struct
				{
					float x, y, z, a;
				};
			};
		};

		DllExport Vector4D Add(const Vector4D& vec1, const Vector4D& vec2);
		DllExport Vector4D Subtract(const Vector4D& vec1, const Vector4D& vec2);



	}
}
#endif