#ifdef _WIN32

#define DllExport __declspec(dllexport)

#else
#define DllExport
#endif

// Cross Platform Common Defines
#ifndef interface
#define interface struct
#endif

#define VIRTUAL NULL

#define IsNull(p) p == nullptr
#define IsNotNull(p) p != nullptr

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

typedef int BOOL;

#ifndef ZeroMemory
#define ZeroMemory(p,s) memset(p, 0, s)
#endif
