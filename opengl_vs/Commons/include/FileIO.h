#ifndef GALASTI_FILE_IO_H
#define GALASTI_FILE_IO_H

#include "stdinc.h"
#include<stdio.h>
#include<cstring>

namespace GApi {
	namespace File {


		BOOL LoadFile(const char* pszFilePath, char* pszFileOutput, unsigned int maxLength)
		{
			FILE* pFile = fopen(pszFilePath, "r+");
			if (IsNull(pFile))
				return FALSE;

			memset(pszFileOutput, 0, maxLength);
			char szLine[1024];
			while (fgets(szLine, 1024, pFile))
				strcat(pszFileOutput, szLine);

			return TRUE;
		}

	}
}


#endif