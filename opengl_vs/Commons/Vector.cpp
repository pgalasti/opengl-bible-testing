#include "Vector.h"

namespace GApi {
	namespace Math {

		Vector4D Add(const Vector4D& vec1, const Vector4D& vec2)
		{
			Vector4D result;

			result.x = vec1.x + vec2.x;
			result.y = vec1.y + vec2.y;
			result.z = vec1.z + vec2.z;
			result.a = vec1.a + vec2.a;

			return result;
		}

		Vector4D Subtract(const Vector4D& vec1, const Vector4D& vec2)
		{
			Vector4D result;

			result.x = vec1.x - vec2.x;
			result.y = vec1.y - vec2.y;
			result.z = vec1.z - vec2.z;
			result.a = vec1.a - vec2.a;

			return result;
		}

	}
}